# SMS Gate
(based on https://github.com/jberkel/sms-backup-plus)

You should be able to control all data produced by phone and be able to collect it all into one place (cloud / p2p or local). SMS Gate app backup all SMS from Android/INCOMING folder to the Imap/INBOX folder (or Gmail) and allows you to reply to the SMS using standard reply e-mail mecanics.

Control your phone SMS's trougth the web page. All application communications protected by Bitcoin Wallet P2P encryption, requires firebase (non open-source libraries) and API11+ for ElipticCurve encryption.

  * https://axet.gitlab.io/android-sms-gate/

# YouTube

  * https://www.youtube.com/watch?v=knmNvtXflOQ

# Manual install

    gradle installDebug

# Screenshots

![shot](/docs/screenshot1.png)

## Links

  * https://gitlab.com/axet/android-media-merger
  * https://syncthing.net/
  * https://pushjet.io/
  * https://www.pushbullet.com/
  * https://www.airdroid.com
