## SMS Backup

Default SMS backup folder set to Imap/INBOX folder. All your incoming SMS will appear in INBOX as Unread messages. You can reply them, SMS Gate will check INBOX folder and send respond to the sender.

After repying, SMS Gate will mark as forwarded the reply email in your mailbox. SMS Gate will not reply to the incoming SMS it do not own (if you have two instances on two phones it helps do not cross sms reply functionality).

Works best with standalone android phones (https://github.com/axet/android-gsm-gate/tree/master/keneksi-sun)

## Notifications Listener

SMS Gate allows you to listen for system notifications and display them at Web page. Android supports it in two ways:

  * AccessibilityService
  * NotificationListenerService

Unfortenatly, NotificationListenerService have a bug [#62811](https://code.google.com/p/android/issues/detail?id=62811) when application restarts / upates NotificationListenerService not get restarted, and new notifications does not apper. You can fix it manually by disabling / enabling SMS gate in service list, or restart phone. Fixed for Android 7.0.

Second option is to use AccessibilityService. It does not allow you to remove notifications from your phone, but it is more stable. To enable AccessibilityService just go to "Settings / Accessibility / SMS Gate = On"

NotificationListenerService available at "Settings / Sound & notification / Advanced / Notification access / SMS Gate = On"

## Reboot

To enable reboot on non rooted phones it has to be API >= 24 and run following adb command:

    # adb shell dpm set-device-owner com.github.axet.smsgate/.services.DeviceAdmin

disable:

    # adb shell am start -n com.github.axet.smsgate/.activities.DisableOwner

## Web Interface

Web interface requires API11+ sice it uses EC for P2P encryption.

## Services

  * SMS Backup have regular schedule set to 1 minute + Incoming SMS event trigs the backup.
  * SMS Reply monitors for Imap Push events read INBOX Imap folder. Have regular schedule 5 minutes.

## Auto Boot device when charger connected

Normally, when android is off, connecting charing starts charging animation. But when you need to process normal boot when charger connected you have to try two options:

1) `fastboot oem off-mode-charge 0`

2) read your `init.*.rc` files and find 'on charger', on my device I have `/init.charging.rc` with `service ipod`, so replace `/system/bin/ipod` with following:

```bash
    #!/system/bin/sh
    /system/bin/reboot
```