package com.github.axet.smsgate.services;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;

import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.app.Storage;

import java.util.Map;

@TargetApi(19) // SMS_DELIVER is API19+
public class IncomingPostReceiver extends BroadcastReceiver { // sms deliver handler (we have to handle storage)
    @Override
    public void onReceive(Context context, Intent intent) {
        String a = intent.getAction();
        if (a == null)
            return;
        if (a.equals(Telephony.Sms.Intents.SMS_DELIVER_ACTION)) {
            Map<String, SmsStorage.Message> map = SmsStorage.getMessagesFromIntent(intent);
            if (map == null)
                return;
            SmsStorage storage = new SmsStorage(context);
            for (String id : map.keySet()) {
                SmsStorage.Message m = map.get(id);
                if (Storage.filter(SMSApplication.from(context).items, m))
                    continue;
                storage.add(m);
            }
        }
    }
}
