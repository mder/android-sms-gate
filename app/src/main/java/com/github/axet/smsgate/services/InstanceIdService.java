package com.github.axet.smsgate.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.zegoggles.smssync.activity.SMSGateFragment;

public class InstanceIdService extends FirebaseInstanceIdService {
    public static final String TAG = InstanceIdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "FCM Token: " + token);

        SMSGateFragment.checkPermissions(this);
        FirebaseService.token(this);
    }
}
