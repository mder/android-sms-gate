package com.github.axet.smsgate.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class OnExternalReceiver extends com.github.axet.androidlibrary.services.OnExternalReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "OnReceive");
        if (!isExternal(context))
            return;
        OnBootReceiver.start(context);
    }
}
