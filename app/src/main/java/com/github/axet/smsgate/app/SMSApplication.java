package com.github.axet.smsgate.app;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.TextBody;
import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.Firebase;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.crypto.Bitcoin;
import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.androidlibrary.services.WifiKeepService;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.smsgate.BuildConfig;
import com.github.axet.smsgate.activities.MainActivity;
import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.providers.SMS;
import com.github.axet.smsgate.services.FirebaseService;
import com.github.axet.smsgate.services.NotificationService;
import com.github.axet.smsgate.services.ScheduleService;
import com.github.axet.smsgate.widgets.ApplicationsPreference;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.mail.Headers;
import com.zegoggles.smssync.preferences.AuthPreferences;
import com.zegoggles.smssync.service.SmsBackupService;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static com.fsck.k9.mail.internet.MimeMessageHelper.setBody;

// adb shell "am broadcast -a com.github.axet.smsgate.key -e key '3333441...2341234'"
// adb shell "am broadcast -a com.github.axet.smsgate.sms -e phone +199988877766 -e msg 'hello'"
public class SMSApplication extends App {
    public static final String SCHEDULER_COUNT = "SCHEDULER_COUNT";
    public static final String SCHEDULER_ITEM = "SCHEDULER_";
    public static final String SEC = "SEC";
    public static final String SMS_LAST = "SMS_LAST";
    public static final String STORAGE_SMS_LAST = "STORAGE_SMS_LAST";
    public static final String PREF_FIREBASE = "firebase";
    public static final String PREF_NOTIFICATION_LISTENER = "notification_listener";
    public static final String PREF_WIFIRESTART = "wifi_restart";
    public static final String PREF_WIFI = "wifi_only";
    public static final String PREF_ADMIN = "admin";
    public static final String PREF_OPTIMIZATION = "optimization";
    public static final String PREF_NEXT = "next";
    public static final String PREF_DEFAULTSMS = "defaultsms";
    public static final String PREF_FIREBASESETTINGS = "firebase_settings";
    public static final String PREF_REBOOT = "reboot";
    public static final String PREF_STORAGE = "storage";
    public static final String PREF_NAMEFORMAT = "name";
    public static final String PREF_DELETE = "delete";
    public static final String PREF_REPLY = "reply"; // reply last query time

    public static final String PREF_OAUTH = "xoauth";

    public static final String PREF_APPS = "applications"; // enabled
    public static final String APPS_INDEX = "APPS_";
    public static final String APPS_COUNT = "APPS_COUNT";

    public static final String APP_FROM = "from";
    public static final String APP_SUBJ = "subject";
    public static final String APP_BODY = "body";

    public static final String ACTION_KEY = BuildConfig.APPLICATION_ID + ".key";
    public static final String ACTION_SENDSMS = BuildConfig.APPLICATION_ID + ".sms";
    public static final String[] COMMANDS = new String[]{ACTION_KEY, ACTION_SENDSMS};

    public static final String[] SEND_PERMISSIONS = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS};

    Thread t;
    SIM sim;

    public BitcoinStorage keys = null;
    public ScheduleSMSStorage items;
    public AppsStorage apps = new AppsStorage();
    public NotificationChannelCompat channel;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getAction();
            if (a != null) {
                if (a.equals(ACTION_KEY)) {
                    keys.load(intent.getStringExtra("key"));
                    keys.save();
                    FirebaseService.start(context);
                }
                if (a.equals(ACTION_SENDSMS)) {
                    if (Storage.permitted(context, SEND_PERMISSIONS))
                        SMS.send(context, intent.getStringExtra("phone"), intent.getStringExtra("msg"));
                    else
                        MainActivity.start(context, SEND_PERMISSIONS, intent);
                }
            }
        }
    };

    public static SMSApplication from(Context context) {
        return (SMSApplication) App.from(context);
    }

    public static boolean firebaseEnabled(Context context) {
        if (Build.VERSION.SDK_INT < 11) // WebInterface requires P2P encryption API11+ EC KeyFactory
            return false;
        return Firebase.isEnabled(context);
    }

    public class BitcoinStorage extends Bitcoin {
        public BitcoinStorage() {
            load();
        }

        public void load() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            String sec = prefs.getString(SEC, "");
            if (sec.isEmpty()) {
                generate();
                save();
            } else {
                load(sec);
            }
        }

        public void save() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(SEC, saveSec());
            edit.commit();
        }
    }

    public class ScheduleSMSStorage extends ArrayList<ScheduleSMS> {
        public ScheduleSMSStorage() {
            load();
        }

        public void load() {
            clear();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            int count = prefs.getInt(SCHEDULER_COUNT, 0);
            for (int i = 0; i < count; i++) {
                String state = prefs.getString(SCHEDULER_ITEM + i, "");
                ScheduleSMS item = new ScheduleSMS(SMSApplication.this, state);
                add(item);
            }
        }

        public void save() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt(SCHEDULER_COUNT, size());
            for (int i = 0; i < size(); i++) {
                ScheduleSMS item = get(i);
                edit.putString(SCHEDULER_ITEM + i, item.save().toString());
            }
            edit.commit();
        }

        public PendingIntent registerNextAlarm() {
            TreeSet<Long> events = new TreeSet<>();
            for (ScheduleSMS s : this) {
                if (s.next != 0 && s.enabled)
                    events.add(s.next);
            }

            Calendar cur = Calendar.getInstance();

            Intent alarmIntent = new Intent(SMSApplication.this, ScheduleService.class).setAction(ScheduleService.ACTION_SMS);

            if (events.isEmpty()) {
                AlarmManager.cancel(SMSApplication.this, alarmIntent);
                return null;
            } else {
                long time = events.first();

                alarmIntent.putExtra("time", time);

                Log.d(TAG, "Current: " + ScheduleSMS.formatDate(cur.getTimeInMillis()) + " " + ScheduleSMS.formatTime(cur.getTimeInMillis()) + "; SetAlarm: " + ScheduleSMS.formatDate(time) + " " + ScheduleSMS.formatTime(time));

                return AlarmManager.setExact(SMSApplication.this, time, alarmIntent);
            }
        }
    }

    public class AppsStorage {
        Handler handler = new Handler();
        NotificationService.NotificationsMap<NotificationInfo> lastId = new NotificationService.NotificationsMap<>(handler);

        public void notification(String action, String pkg, String id, Notification n) {
            if (Build.VERSION.SDK_INT >= 16) {
                if (n.priority <= Notification.PRIORITY_LOW)
                    return; // ignore low priority notifications
            }
            if (action.equals(NotificationService.REMOVE)) {
                lastId.remove(pkg);
                return;
            }
            AuthPreferences auth = new AuthPreferences(SMSApplication.this);
            boolean connected = auth.hasOauthTokens() || auth.hasOAuth2Tokens();
            if (!connected)
                return;
            add(pkg, n);
        }

        public void add(String pkg, Notification n) {
            String applicationName = FirebaseService.getApplicationName(SMSApplication.this, pkg);

            String title = null;
            String text;
            String details = null;

            if (Build.VERSION.SDK_INT >= 19) {
                title = FirebaseService.toString(n.extras.get("android.title"));
                text = FirebaseService.toString(n.extras.get("android.text")); // can be spannable
                if (text == null || text.isEmpty()) {
                    text = FirebaseService.toString(n.tickerText); // can be null
                }
                details = FirebaseService.toString(n.extras.get("android.bigText")); // class android.text.SpannableString
            } else {
                text = FirebaseService.toString(n.tickerText); // can be null
            }

            if (title == null || title.isEmpty()) {
                title = text;
                if (title == null || title.isEmpty())
                    title = details;
                if (title == null || title.isEmpty())
                    title = "";
                if (details == null || details.isEmpty())
                    details = text;
                if (details == null || details.isEmpty())
                    details = "";
            } else if (details == null || details.isEmpty()) {
                details = text;
                if (details == null || details.isEmpty())
                    details = "";
            }

            long now = System.currentTimeMillis();

            if (Build.VERSION.SDK_INT >= 18) { // old api has only UPDATE events and no REMOVE, add them all
                int code = (title + " " + details).hashCode();
                NotificationInfo last = lastId.get(pkg);
                if (last == null)
                    last = new NotificationInfo(pkg);

                if (lastId.duplicate(last, code))
                    return; // do not email same text twice

                last.pkg = pkg;
                last.n = n;

                if (lastId.delayed(last, now)) {
                    final NotificationInfo info = last;
                    lastId.delay(last, new Runnable() {
                        @Override
                        public void run() {
                            add(info.pkg, info.n);
                        }
                    });
                    return;
                }

                lastId.put(last, code, now);
            }

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            Set<String> ss = ApplicationsPreference.load(prefs.getString(SMSApplication.PREF_APPS, ""));
            if (!ApplicationsPreference.contains(ss, pkg))
                return;
            int i = prefs.getInt(APPS_COUNT, 0);

            for (int j = 0; j < i; j++) {
                try {
                    String json = prefs.getString(APPS_INDEX + j, "");
                    JSONObject o = new JSONObject(json);
                    String from = o.getString(APP_FROM);
                    String subj = o.getString(APP_SUBJ);
                    String body = o.getString(APP_BODY);
                    if (from.equals(applicationName) && subj.equals(title) && body.equals(details))
                        return;// duplicate
                } catch (JSONException e) {
                    Log.d(TAG, "unable to process message", e); // ignore
                }
            }

            int count = i + 1;

            try {
                JSONObject json = new JSONObject();
                json.put(APP_FROM, applicationName);
                json.put(APP_SUBJ, title);
                json.put(APP_BODY, details);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString(APPS_INDEX + i, json.toString());
                edit.putInt(APPS_COUNT, count);
                edit.commit();
            } catch (JSONException e) {
                Log.d(TAG, "unable to process message", e); // ignore
            }

            SmsBackupService.scheduleBackup(SMSApplication.this, 3); // 3 sec delay
        }

        public List<Message> getMessages(String userEmail) {
            ArrayList<Message> list = new ArrayList<>();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            int count = prefs.getInt(APPS_COUNT, 0);
            for (int i = 0; i < count; i++) {
                try {
                    final Message msg = new MimeMessage();
                    String json = prefs.getString(APPS_INDEX + i, "");
                    JSONObject o = new JSONObject(json);
                    String from = o.getString(APP_FROM);
                    String subj = o.getString(APP_SUBJ);
                    msg.setFrom(new Address(userEmail, from));
                    msg.setRecipient(Message.RecipientType.TO, new Address(userEmail));
                    msg.setSubject(subj);
                    msg.setHeader(Headers.REFERENCES, String.format("<%d.%d@sms-backup-plus.local>", from.hashCode(), subj.hashCode()));
                    setBody(msg, new TextBody(o.getString(APP_BODY)));
                    list.add(msg);
                } catch (Exception e) {
                    Log.d(TAG, "unable to process message", e); // ignore
                }
            }

            return list;
        }

        public int getCount() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            return prefs.getInt(APPS_COUNT, 0);
        }

        public int deleteAll() {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SMSApplication.this);
            int count = prefs.getInt(APPS_COUNT, 0);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt(APPS_COUNT, 0);
            edit.commit();
            return count;
        }
    }

    public static class NotificationInfo extends NotificationService.NotificationInfo {
        String pkg;
        String nid;
        Notification n;

        public NotificationInfo(String id) {
            super(id);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Firebase.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        channel = new NotificationChannelCompat(this, "status", "Status", NotificationManagerCompat.IMPORTANCE_LOW);

        if (Build.VERSION.SDK_INT >= 11)
            keys = new BitcoinStorage(); // min API11+ EC KeyFactory

        items = new ScheduleSMSStorage();

        IntentFilter ff = new IntentFilter();
        for (String c : COMMANDS)
            ff.addAction(c);
        registerReceiver(receiver, ff);
    }

    public SIM getSIM() {
        if (sim == null)
            sim = new SIM(this);
        return sim;
    }

    public static Uri shareFile(Context context, String type, String name, byte[] buf) {
        try {
            File outputDir = context.getCacheDir();
            File outputFile = File.createTempFile("share", ".tmp", outputDir);
            IOUtils.write(buf, new FileOutputStream(outputFile));
            return FileProvider.getProvider().share(type, name, outputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void shareClear(Context context) {
        File outputDir = context.getCacheDir();
        File[] ff = outputDir.listFiles();
        if (ff != null) {
            for (File f : ff) {
                if (f.getName().startsWith("share"))
                    f.delete();
            }
        }
    }

    public static String toHexString(int l) {
        return String.format("%08X", l);
    }

    public static String toHexString(long l) {
        return String.format("%016X", l);
    }

    public boolean wifi() {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        t = WifiKeepService.wifi(this, ScheduleService.class, shared.getBoolean(SMSApplication.PREF_WIFIRESTART, false));
        return t != null;
    }
}
