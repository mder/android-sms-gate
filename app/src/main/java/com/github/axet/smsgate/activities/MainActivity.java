package com.github.axet.smsgate.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.dialogs.ShareDialogFragment;
import com.github.axet.smsgate.dialogs.ShareIncomingFragment;
import com.github.axet.smsgate.fragments.SchedulersFragment;
import com.github.axet.smsgate.fragments.SettingsFragment;
import com.github.axet.smsgate.services.OnBootReceiver;
import com.github.axet.smsgate.services.ScheduleService;
import com.github.axet.smsgate.widgets.ViewPagerDisabled;
import com.zegoggles.smssync.activity.SMSGateFragment;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements DialogInterface.OnDismissListener {
    public static String SHARE = MainActivity.class.getCanonicalName() + ".SHARE";
    public static String PERMS = MainActivity.class.getCanonicalName() + ".PERMS";

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    public static final int SHARE_PERMS = 1;
    public static final int RESULT_PERMS = 2;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    OnBackHandler backHandler;

    ViewPagerDisabled mViewPager;
    TabLayout tabLayout;
    AppBarLayout appbar;
    Handler handler = new Handler();
    Intent permsIntent;

    boolean resumeShare;
    Bundle shareArgs;

    public static void start(Context context, String[] pp, Intent i) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction(PERMS);
        intent.putExtra("perms", pp);
        intent.putExtra("intent", i);
        context.startActivity(intent);
    }

    public interface OnBackHandler {
        void onBackPressed();
    }

    public static class SettingsTabView extends AppCompatImageView {
        Drawable d;

        public SettingsTabView(Context context, TabLayout.Tab tab, ColorStateList colors) {
            super(context);

            d = ContextCompat.getDrawable(context, R.drawable.ic_more_vert_black_24dp);

            setImageDrawable(d);

            setColorFilter(colors.getDefaultColor());
        }

        void updateLayout() {
            ViewParent p = getParent();
            if (p != null && p instanceof LinearLayout) { // TabView extends LinearLayout
                LinearLayout l = (LinearLayout) p;
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) l.getLayoutParams();
                if (lp != null) {
                    lp.weight = 0;
                    lp.width = LinearLayout.LayoutParams.WRAP_CONTENT;

                    int left = l.getMeasuredHeight() / 2 - d.getIntrinsicWidth() / 2;
                    int right = left;
                    left -= l.getPaddingLeft();
                    right -= l.getPaddingRight();
                    if (left < 0)
                        left = 0;
                    if (right < 0)
                        right = 0;
                    setPadding(left, 0, right, 0);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        if (OptimizationPreferenceCompat.needKillWarning(this, SMSApplication.PREF_NEXT))
            OptimizationPreferenceCompat.buildKilledWarning(this, true, SMSApplication.PREF_OPTIMIZATION, OptimizationPreferenceCompat.forceInit(ScheduleService.class)).show();

        OnBootReceiver.main(this);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        appbar = (AppBarLayout) findViewById(R.id.appbar);

        mViewPager = (ViewPagerDisabled) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        setupTabs();

        openIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        openIntent(intent);
    }

    void openIntent(Intent intent) {
        if (intent == null)
            return;
        String a = intent.getAction();
        if (a == null)
            return;

        if (a.equals(Intent.ACTION_SEND)) {
            Uri uri = intent.getParcelableExtra(Intent.EXTRA_STREAM);

            ArrayList<Uri> uu = null;
            if (uri != null) {
                uu = new ArrayList<>();
                uu.add(uri);
            }

            String text = intent.getStringExtra(Intent.EXTRA_TEXT);
            if (text == null) {
                text = intent.getStringExtra(Intent.EXTRA_SUBJECT);
            }

            shareArgs = new Bundle();
            shareArgs.putString("text", text);
            shareArgs.putParcelableArrayList("uri", uu);

            if (uri == null || Storage.permitted(this, PERMISSIONS))
                shareDialog(shareArgs);
        }

        if (a.equals(Intent.ACTION_SEND_MULTIPLE)) {
            ArrayList<Uri> uu = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);

            String text = intent.getStringExtra(Intent.EXTRA_TEXT);
            if (text == null) {
                text = intent.getStringExtra(Intent.EXTRA_SUBJECT);
            }

            shareArgs = new Bundle();
            shareArgs.putString("text", text);
            shareArgs.putParcelableArrayList("uri", uu);

            if (Storage.permitted(this, PERMISSIONS, SHARE_PERMS)) {
                shareDialog(shareArgs);
            }
        }

        if (a.equals(SHARE)) {
            android.support.v4.app.FragmentManager fm = this.getSupportFragmentManager();
            ShareIncomingFragment dialog = new ShareIncomingFragment();
            dialog.setArguments(intent.getExtras());
            dialog.show(fm, "");
        }

        if (a.equals(PERMS)) {
            Storage.permitted(this, intent.getStringArrayExtra("perms"), RESULT_PERMS);
            permsIntent = intent.getParcelableExtra("intent");
        }
    }

    void setupTabs() {
        tabLayout.setupWithViewPager(mViewPager);
        TabLayout.Tab tab = tabLayout.getTabAt(2);
        SettingsTabView v = new SettingsTabView(this, tab, tabLayout.getTabTextColors());
        tab.setCustomView(v);
        v.updateLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar base clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        HashMap<Integer, Fragment> map = new HashMap<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SMSGateFragment();
                case 1:
                    return new SchedulersFragment();
                case 2:
                    return new SettingsFragment();
                default:
                    throw new RuntimeException("bad page");
            }
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object o = super.instantiateItem(container, position);
            map.put(position, (Fragment) o);
            return o;
        }

        public Fragment getFragment(int pos) {
            return map.get(pos);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.app_name);
                case 1:
                    return "Schedulers";
                case 2:
                    return "⋮";
                default:
                    throw new RuntimeException("bad page");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (resumeShare) {
            shareDialog(shareArgs);
            resumeShare = false;
        }
    }

    @Override
    public void onBackPressed() {
        if (backHandler != null) {
            backHandler.onBackPressed();
            return;
        }
        super.onBackPressed();
    }

    public void onBackHandler(OnBackHandler h) {
        backHandler = h;
        if (h == null) {
            mViewPager.enabled = true;
            handler.post(new Runnable() {
                @Override
                public void run() {
                    tabLayout.setTabMode(TabLayout.MODE_FIXED);
                    tabLayout.setOnTabSelectedListener(null);
                    setupTabs();
                }
            });
            return;
        }
        mViewPager.enabled = false;
        handler.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                tabLayout.removeAllTabs();
                final TabLayout.Tab back = tabLayout.newTab();
                back.setCustomView(R.layout.back);
                tabLayout.clearOnTabSelectedListeners();
                tabLayout.addTab(back, true);
                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        if (tab == back)
                            onBackPressed();
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        if (tab == back)
                            onBackPressed();
                    }
                });
            }
        });
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        int i = mViewPager.getCurrentItem();
        Fragment f = mSectionsPagerAdapter.getFragment(i);
        if (f instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) f).onDismiss(dialog);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void shareDialog(Bundle args) {
        android.support.v4.app.FragmentManager fm = this.getSupportFragmentManager();
        ShareDialogFragment dialog = new ShareDialogFragment();
        dialog.setArguments(args);
        dialog.show(fm, "");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Storage.permitted(this, permissions)) {
            switch (requestCode) {
                case SHARE_PERMS:
                    resumeShare = true;
                    break;
                case RESULT_PERMS:
                    if (permsIntent != null) {
                        sendBroadcast(permsIntent);
                        permsIntent = null;
                    }
                    break;
            }
        } else {
            Toast.makeText(this, R.string.not_permitted, Toast.LENGTH_SHORT).show();
        }
    }
}
